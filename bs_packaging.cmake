#--------------------------------------------------------------#
#                Bred-Soft base packaging module               #
#--------------------------------------------------------------#

# --- TARGET_ARCHITECTURE (thanks "https://github.com/axr/solar-cmake" for folloved code):

if("${TARGET_ARCHITECTURE}" STREQUAL "")
    if(APPLE AND CMAKE_OSX_ARCHITECTURES)
        foreach(osx_arch ${CMAKE_OSX_ARCHITECTURES})
            if("${osx_arch}" STREQUAL "ppc" AND ppc_support)
                set(osx_arch_ppc TRUE)
            elseif("${osx_arch}" STREQUAL "i386")
                set(osx_arch_i386 TRUE)
            elseif("${osx_arch}" STREQUAL "x86_64")
                set(osx_arch_x86_64 TRUE)
            elseif("${osx_arch}" STREQUAL "ppc64" AND ppc_support)
                set(osx_arch_ppc64 TRUE)
            else()
                message(FATAL_ERROR "Invalid OS X arch name: ${osx_arch}")
            endif()
        endforeach()
        
        if(osx_arch_ppc)
            list(APPEND TARGET_ARCHITECTURE ppc)
        endif()
        
        if(osx_arch_i386)
            list(APPEND TARGET_ARCHITECTURE i386)
        endif()
        
        if(osx_arch_x86_64)
            list(APPEND TARGET_ARCHITECTURE amd64)
        endif()
        
        if(osx_arch_ppc64)
            list(APPEND TARGET_ARCHITECTURE ppc64)
        endif()
    else()
        file(WRITE "${CMAKE_BINARY_DIR}/arch.c" "
#if defined(__arm__) || defined(__TARGET_ARCH_ARM)
#  if defined(__ARM_ARCH_7__) || defined(__ARM_ARCH_7A__) || \\
      defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || \\
      (defined(__TARGET_ARCH_ARM) && __TARGET_ARCH_ARM-0 >= 7)
#    error cmake_ARCH armv7
#  elif defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || \\
        defined(__ARM_ARCH_6T2__) || defined(__ARM_ARCH_6Z__) || \\
        defined(__ARM_ARCH_6K__) || defined(__ARM_ARCH_6ZK__) || \\
        defined(__ARM_ARCH_6M__) || \\
        (defined(__TARGET_ARCH_ARM) && __TARGET_ARCH_ARM-0 >= 6)
#    error cmake_ARCH armv6
#  elif defined(__ARM_ARCH_5TEJ__) || \\
        (defined(__TARGET_ARCH_ARM) && __TARGET_ARCH_ARM-0 >= 5)
#    error cmake_ARCH armv5
#  else
#    error cmake_ARCH arm
#  endif
#elif defined(__i386) || defined(__i386__) || defined(_M_IX86)
#  error cmake_ARCH i386
#elif defined(__x86_64) || defined(__x86_64__) || defined(__amd64) || defined(_M_X64)
#  error cmake_ARCH amd64
#elif defined(__ia64) || defined(__ia64__) || defined(_M_IA64)
#  error cmake_ARCH ia64
#elif defined(__ppc__) || defined(__ppc) || defined(__powerpc__) || \\
      defined(_ARCH_COM) || defined(_ARCH_PWR) || defined(_ARCH_PPC) || \\
      defined(_M_MPPC) || defined(_M_PPC)
#  if defined(__ppc64__) || defined(__powerpc64__) || defined(__64BIT__)
#    error cmake_ARCH ppc64
#  else
#    error cmake_ARCH ppc
#  endif
#endif

#error cmake_ARCH unknown
"       )
        
        enable_language(C)
        try_run(
            run_result_unused
            compile_result_unused
            "${CMAKE_BINARY_DIR}"
            "${CMAKE_BINARY_DIR}/arch.c"
            COMPILE_OUTPUT_VARIABLE TARGET_ARCHITECTURE
            CMAKE_FLAGS CMAKE_OSX_ARCHITECTURES=${CMAKE_OSX_ARCHITECTURES}
        )
        
        string(REGEX MATCH "cmake_ARCH ([a-zA-Z0-9_]+)"
               TARGET_ARCHITECTURE "${TARGET_ARCHITECTURE}"
        )
        string(REPLACE "cmake_ARCH " ""
               TARGET_ARCHITECTURE "${TARGET_ARCHITECTURE}"
        )
        
        if(NOT TARGET_ARCHITECTURE)
            set(TARGET_ARCHITECTURE "unknown")
        endif()
    endif()
endif() #- TARGET_ARCHITECTURE

# ---

if("${CPACK_PACKAGE_NAME}" STREQUAL "")
    string(TOLOWER "${PROJECT_NAME}" CPACK_PACKAGE_NAME)
endif()

if("${CPACK_PACKAGE_VERSION}" STREQUAL "")
    set(CPACK_PACKAGE_VERSION "${PROJECT_PURE_VERSION}")
    
    if(PROJECT_VERSION_MAJOR)
        set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
        
        if(PROJECT_VERSION_MINOR)
            set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
            
            if(PROJECT_VERSION_PATCH)
                set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
            endif()
        endif()
    endif()
endif()

if("${CPACK_COMPONENT_INSTALL}" STREQUAL "")
    set(CPACK_COMPONENT_INSTALL OFF)
endif()


# ---

function(finish_pack)
    if("${CPACK_PACKAGING_INSTALL_PREFIX}" STREQUAL "")
        set(CPACK_PACKAGING_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}" PARENT_SCOPE)
    endif()
    
    if("${CPACK_PACKAGE_FILE_NAME}" STREQUAL "")
        set(CPACK_PACKAGE_FILE_NAME
            "${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}_${TARGET_ARCHITECTURE}"
            PARENT_SCOPE
        )
    endif()
    
    if("${CPACK_ARCHIVE_COMPONENT_INSTALL}" STREQUAL "")
        set(CPACK_ARCHIVE_COMPONENT_INSTALL ${CPACK_COMPONENT_INSTALL} PARENT_SCOPE)
    endif()
    
    set(CPACK_COMPONENTS_GROUPING IGNORE PARENT_SCOPE)
    set(CPACK_COMPONENT_BINARY_REQUIRED ON PARENT_SCOPE)
    set(CPACK_COMPONENT_DEVELOP_DEPENDS "Binary" PARENT_SCOPE)
    
    include(CPack)
endfunction(finish_pack)

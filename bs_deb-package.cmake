#--------------------------------------------------------------#
#               Bred-Soft Debian packaging module              #
#--------------------------------------------------------------#

if("${CPACK_DEBIAN_PACKAGE_VERSION}" STREQUAL "")
    set(CPACK_DEBIAN_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION}")
endif()

if("${CPACK_DEBIAN_PACKAGE_ARCHITECTURE}" STREQUAL "")
    set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "${TARGET_ARCHITECTURE}")
endif()

list(APPEND CPACK_GENERATOR DEB)

set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)


# ---

macro(__parse_control_file fname_ component_infix_)
    set(field_var "")
    
    file(STRINGS "${fname_}" control__ ENCODING UTF-8)
    foreach(line__ ${control__})
        if(line__ MATCHES "^\\s*$")
            continue()
        endif()
        
        if(line__ MATCHES "^\\s")
            if(NOT field_var)
                message(ERROR "Error in control file '${fname_}'!")
                return()
            endif()
            
            set(${field_var} "${${field_var}}\n${line__}")
            set(${field_var} "${${field_var}}" PARENT_SCOPE)
            continue()
        endif()
        
        string(FIND "${line__}" ":" separator_position)
        if(NOT separator_position)
            continue()
        endif()
        
        string(SUBSTRING "${line__}" 0 ${separator_position} field_name)
        
        if("${field_name}" STREQUAL "Package")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_NAME")
        elseif("${field_name}" STREQUAL "Source")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_SOURCE")
        elseif("${field_name}" STREQUAL "Version")
            set(field_var "CPACK_DEBIAN_PACKAGE_VERSION")
        elseif("${field_name}" STREQUAL "Architecture")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_ARCHITECTURE")
        elseif("${field_name}" STREQUAL "Maintainer")
            set(field_var "CPACK_DEBIAN_PACKAGE_MAINTAINER")
        elseif("${field_name}" STREQUAL "Homepage")
            set(field_var "CPACK_DEBIAN_PACKAGE_HOMEPAGE")
        
        elseif("${field_name}" STREQUAL "Description")
            if("${component_infix_}" STREQUAL "")
                set(field_var "CPACK_DEBIAN_PACKAGE_DESCRIPTION")
            else()
                set(field_var "CPACK_COMPONENT_${component_infix_}DESCRIPTION")
            endif()
        
        elseif("${field_name}" STREQUAL "Section")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_SECTION")
        elseif("${field_name}" STREQUAL "Priority")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_PRIORITY")
        elseif("${field_name}" STREQUAL "Pre-Depends")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_PREDEPENDS")
        elseif("${field_name}" STREQUAL "Depends")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_DEPENDS")
        elseif("${field_name}" STREQUAL "Enhances")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_ENHANCES")
        elseif("${field_name}" STREQUAL "Breaks")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_BREAKS")
        elseif("${field_name}" STREQUAL "Conflicts")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_CONFLICTS")
        elseif("${field_name}" STREQUAL "Provides")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_PROVIDES")
        elseif("${field_name}" STREQUAL "Replaces")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_REPLACES")
        elseif("${field_name}" STREQUAL "Recommends")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_RECOMMENDS")
        elseif("${field_name}" STREQUAL "Suggests")
            set(field_var "CPACK_DEBIAN_${component_infix_}PACKAGE_SUGGESTS")
        else()
            continue()
        endif()
        
        math(EXPR separator_position "${separator_position} + 1")
        string(LENGTH "${line__}" length__)
        math(EXPR length__ "${length__} - ${separator_position}")
        
        string(SUBSTRING "${line__}" ${separator_position} ${length__} field_value)
        string(STRIP "${field_value}" field_value)
        
        set(${field_var} "${field_value}")
        set(${field_var} "${field_value}" PARENT_SCOPE)
    endforeach(line__)
endmacro(__parse_control_file)

# ---

function(deb_package control_dir_) # component_
    string(REGEX REPLACE "/$" "" control_dir_ "${control_dir_}")
    get_filename_component(control_dir_ "${control_dir_}" ABSOLUTE)
    
    if("${ARGC}" GREATER 1)
        set(CPACK_DEB_COMPONENT_INSTALL ON PARENT_SCOPE)
        string(TOUPPER "${ARGV1}_" component_infix_)
    else()
        set(CPACK_DEB_COMPONENT_INSTALL OFF PARENT_SCOPE)
        set(component_infix_ "")
    endif()
    
    # Add and parse files from directory
    
    set(CPACK_DEBIAN_${component_infix_}PACKAGE_CONTROL_EXTRA "")
    
    file(GLOB files__ RELATIVE "${control_dir_}/" "${control_dir_}/*")
    foreach(file__ ${files__})
        if("${file__}" STREQUAL "control")
            __parse_control_file("${control_dir_}/${file__}" "${component_infix_}")
        else()
            list(APPEND CPACK_DEBIAN_${component_infix_}PACKAGE_CONTROL_EXTRA
                "${control_dir_}/${file__}"
            )
        endif()
    endforeach(file__)
    
    set(CPACK_DEBIAN_${component_infix_}PACKAGE_CONTROL_EXTRA
        "${CPACK_DEBIAN_${component_infix_}PACKAGE_CONTROL_EXTRA}"
        PARENT_SCOPE
    )
    set(CPACK_DEBIAN_${component_infix_}PACKAGE_CONTROL_STRICT_PERMISSION
        ON PARENT_SCOPE
    )
    
    # Fix package name
    
    if("${CPACK_DEBIAN_${component_infix_}PACKAGE_NAME}" STREQUAL "")
        set(__package_name "${CPACK_PACKAGE_NAME}")
        set(CPACK_DEBIAN_${component_infix_}PACKAGE_NAME
            "${CPACK_PACKAGE_NAME}" PARENT_SCOPE
        )
    else()
        set(__package_name "${CPACK_DEBIAN_${component_infix_}PACKAGE_NAME}")
    endif()
    
    # Fix file name
    
    if("${CPACK_DEBIAN_PACKAGE_VERSION}" STREQUAL "")
        set(__version "")
    else()
        set(__version "${CPACK_DEBIAN_PACKAGE_VERSION}_")
    endif()
    
    if("${CPACK_DEBIAN_${component_infix_}PACKAGE_ARCHITECTURE}" STREQUAL "")
        set(__arch "${CPACK_DEBIAN_PACKAGE_ARCHITECTURE}")
    else()
        set(__arch "${CPACK_DEBIAN_${component_infix_}PACKAGE_ARCHITECTURE}")
    endif()
    
    set(CPACK_DEBIAN_${component_infix_}FILE_NAME
        "${__package_name}_${__version}${__arch}.deb" PARENT_SCOPE
    )
    
    # Auto-add dependency from "Binary" package to "Develop"
    
    if("${ARGV1}" STREQUAL "Develop")
        if(NOT ("${CPACK_DEBIAN_BINARY_PACKAGE_NAME}" STREQUAL ""))
            set(depend__ "${CPACK_DEBIAN_BINARY_PACKAGE_NAME}")
        elseif(NOT ("${CPACK_DEBIAN_PACKAGE_NAME}" STREQUAL ""))
            set(depend__ "${CPACK_DEBIAN_PACKAGE_NAME}")
        else()
            set(depend__ "${CPACK_PACKAGE_NAME}")
        endif()
        
        if(NOT ("${CPACK_DEBIAN_PACKAGE_VERSION}" STREQUAL ""))
            set(depend__ "${depend__} (>=${CPACK_DEBIAN_PACKAGE_VERSION})")
        endif()
        
        if("${CPACK_DEBIAN_${component_infix_}PACKAGE_DEPENDS}" STREQUAL "")
            set(CPACK_DEBIAN_${component_infix_}PACKAGE_DEPENDS
                "${depend__}" PARENT_SCOPE
            )
        else()
            set(CPACK_DEBIAN_${component_infix_}PACKAGE_DEPENDS
                "${CPACK_DEBIAN_${component_infix_}PACKAGE_DEPENDS}, ${depend__}"
                PARENT_SCOPE
            )
        endif()
    endif()
endfunction(deb_package)

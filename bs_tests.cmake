#--------------------------------------------------------------#
#                  Bred-Soft module for tests                  #
#--------------------------------------------------------------#

set(TEST_DIR "${PROJECT_SOURCE_DIR}/test")


if(DEFINED __BredSoft_included) # - include guard
    return()
endif()


include(CTest)

# ---

function(get_tests)
    if(NOT EXISTS "${TEST_DIR}")
        file(MAKE_DIRECTORY "${TEST_DIR}")
    endif()
    
    if(BUILD_TESTING AND EXISTS "${TEST_DIR}/CMakeLists.txt")
        add_subdirectory("${TEST_DIR}")
    endif()
endfunction(get_tests)

# ---

function(add_test_exe target_name_) # libraries_...
    file(GLOB target_files__
        "${CMAKE_CURRENT_SOURCE_DIR}/${target_name_}.*"
    )
    
    add_executable(${target_name_} ${target_files__})
    if(NOT ("${ARGN}" STREQUAL ""))
        target_link_libraries(${target_name_} ${ARGN})
    endif()
    
    string(MAKE_C_IDENTIFIER "${target_name_}" target_identifier__)
    set(${target_identifier__}_RUNS 0 PARENT_SCOPE)
endfunction(add_test_exe)

# ---

function(add_test_run target_name_) # params_...
    string(MAKE_C_IDENTIFIER "${target_name_}" target_identifier__)
    
    math(EXPR runs__ "${${target_identifier__}_RUNS} + 1")
    set(${target_identifier__}_RUNS ${runs__} PARENT_SCOPE)
    set(run_name "${target_identifier__}_run${runs__}")
    
    add_test(NAME ${run_name}
        COMMAND ${target_name_} ${ARGN}
        WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
    )
endfunction(add_test_run)

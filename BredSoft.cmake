#============================================================================#
#          CMake module provided common Bred-Soft project structure,         #
#                      helping functions and variables.                      #
#============================================================================#
# 
# Must be included before `project` command and setting pure version.
# 
# Mandatory project directories is following:
#   bin/     - for output compiled binaries;
#   lib/     - for output compiled libraries (static and shared);
#   config/  - for configurable files (with ".in" extention). Must repeats
#              project structure;
#   include/ - for independed and configured headers;
#   src/     - for source code of project;
#   test/    - for source code of tests.
# 
# --- Structure:
# 
# PROJECT_MAJOR_NAME, <PROJECT>_MAJOR_NAME
#   - project name with "-${PROJECT_MAJOR_VERSION}" extension, or just project
#     name if project version not provided.
# 
# PROJECT_PURE_VERSION, <PROJECT>_PURE_VERSION
#   - more verbose project version string (used for packages).
#     Sets to "${PROJECT_VERSION}" by default.
# 
# BIN_DIR, LIB_DIR, CONFIG_DIR, INCLUDE_DIR, SRC_DIR
#   - appropriate subdirectories.
# 
# BIN_DESTINATION, LIB_DESTINATION, INCLUDE_DESTINATION
#   - destination properties for install routines.
# 
# STRUCTURE_ALL_IN_ROOT
#   - boolean flag that say use "bin", "lib", etc. subdirectories in root
#     project even for subprojects. Otherwise they will be created separately
#     for each subproject. Sets to "YES" by default.
# 
# forward(<var> [<value> [CACHE <type> <docstring> [FORCE]]])
#   - sets <var> to <value> (if specified) and adding appropriate macro
#     definition for compiler.
# 
# forward_string(<var> [<value> [CACHE <type> <docstring> [FORCE]]])
#   - sets <var> to <value> (if specified) and adding appropriate string
#     literal macro definition for compiler.
# 
# install_directory(<dir> [DESTINATION <dest_dir>] [COMPONENT <component>])
#   - specify installation of <dir>. If no destination specified, then
#     installed in "${CMAKE_INSTALL_PREFIX}/<dir>", else installed like with
#     regular DESTINATION option. Installed to "<component>" component or to
#     "Binary" component by default. Sets variable "<dir>_DIR" (and forward
#     it) depend on CMAKE_BUILD_TYPE (in-project path for "Debug" and
#     destination path for others).
# 
# get_sources()
#   - automatic configuring and calling "src/CMakeLists.txt" file if it
#     exists.
# 
# --- Resources:
# 
# RES_INCLUDE_DIR
#   - directory for generated headers files.
# 
# add_resource_library(<name> <file> [<file> ...])
#   - add a static library that contains specified files (in raw form).
#     Include `<name>.h` to import them.
# 
# --- Targets:
# 
# EXPORT_DESTINATION
#   - destination for install exporting files.
# 
# target_resources(<target> <file> [<file> ...])
#   - specify resources, that must be installed with the target. The files
#     will be installed to "${CMAKE_INSTALL_PREFIX}/<path_inside_project>".
#     For "Develop" component by default.
# 
# target_public_headers(<target> <file> [<file> ...])
#   - specify headers files, that must be installed with target (commonly
#     library). For "Develop" component by default.
# 
# common_install(<target> [DESTINATION <destination>] [COMPONENT <component>]
#                [NO_VERSION] [OPTIONAL])
#   - install target with common destination and component (if they not
#     specified), plus resources and public headers saving folders structure.
#     Default component is "Develop" for archives, resources and public
#     headers and "Binary" for other. "NO_VERSION" for suppress version suffix
#     in output name. "OPTIONAL" forwards through.
# 
# export_set(<var> [<value>])
#   - set variable named "<var>" in export config file for value "<value>" or
#     for actual variable value by default.
# 
# install_export([MAIN <component>] [ADDITIONAL <component> ...])
#   - include CMake exporting files to installation, for finding this package
#     from another CMake-project. Main files (which includes exports of
#     default-componented targets) installed for "Develop" component by
#     default. Additional files exports targets of and installed for specified
#     components, if any.
# 
# --- Tests:
# 
# TEST_DIR
#   - directory with source code of tests.
# 
# get_tests()
#   - calling "test/CMakeLists.txt" file if it exists.
# 
# add_test_exe(<target_name> [<library> ...])
#   - add executable target with sources "<target_name>.*" and link libraries
#     to it, if specified.
# 
# add_test_run(<target_name> [<run_arg> ...])
#   - add another test run for target (working directory is project root).
#     Test name will be "<target_name>_run<N>".
# 
# --- Packaging (only for root project):
# 
# TARGET_ARCHITECTURE
#   - codename of detected build architecture.
# 
# deb_package(<control_dir> [<component>])
#   - get extra control-files from specified directory and tuning other
#     debian-package variables from "control" file. If specified, make tunings
#     for <component> package (component install enabled automatically).
#     "Package", "Version" and "Architecture" has default values.
# 
# finish_pack()
#   - finish configuring packages.

cmake_minimum_required(VERSION 3.1)


include(${CMAKE_CURRENT_LIST_DIR}/bs_structure.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bs_resources.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bs_targets.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bs_tests.cmake)

# ---

if(DEFINED __BredSoft_included) # - include guard
    return()
endif()
set(__BredSoft_included ON)

# ---

include(${CMAKE_CURRENT_LIST_DIR}/bs_packaging.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/bs_deb-package.cmake)

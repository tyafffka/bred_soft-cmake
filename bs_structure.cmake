#--------------------------------------------------------------#
#              Bred-Soft project structure module              #
#--------------------------------------------------------------#

if(PROJECT_VERSION)
    set(PROJECT_MAJOR_NAME "${PROJECT_NAME}-${PROJECT_VERSION_MAJOR}")
else()
    set(PROJECT_MAJOR_NAME "${PROJECT_NAME}")
endif()

set(${PROJECT_NAME}_MAJOR_NAME "${PROJECT_MAJOR_NAME}")

if(NOT ("${${PROJECT_NAME}_PURE_VERSION}" STREQUAL ""))
    set(PROJECT_PURE_VERSION "${${PROJECT_NAME}_PURE_VERSION}")
elseif(NOT ("${PROJECT_PURE_VERSION}" STREQUAL ""))
    set(${PROJECT_NAME}_PURE_VERSION "${PROJECT_PURE_VERSION}")
else()
    set(PROJECT_PURE_VERSION "${PROJECT_VERSION}")
    set(${PROJECT_NAME}_PURE_VERSION "${PROJECT_VERSION}")
endif()


if(DEFINED __BredSoft_included AND STRUCTURE_ALL_IN_ROOT) # - include guard
    return()
endif()


set(BIN_DIR     "${PROJECT_SOURCE_DIR}/bin")
set(LIB_DIR     "${PROJECT_SOURCE_DIR}/lib")
set(CONFIG_DIR  "${PROJECT_SOURCE_DIR}/config")
set(INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")
set(SRC_DIR     "${PROJECT_SOURCE_DIR}/src")

foreach(dir_ "${BIN_DIR}" "${LIB_DIR}") # "${CONFIG_DIR}" "${INCLUDE_DIR}" "${SRC_DIR}"
    if(NOT EXISTS "${dir_}")
        file(MAKE_DIRECTORY "${dir_}")
    endif()
endforeach(dir_)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${BIN_DIR}/")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${LIB_DIR}/")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${LIB_DIR}/")

link_directories("${LIB_DIR}/")
include_directories("${INCLUDE_DIR}/" "${SRC_DIR}/")


if(DEFINED __BredSoft_included) # - include guard
    return()
endif()


set(BIN_DESTINATION     "bin")
set(LIB_DESTINATION     "lib")
set(INCLUDE_DESTINATION "include")

set(STRUCTURE_ALL_IN_ROOT YES)

# ---

function(forward var_) # value_ CACHE type_ docstring_ FORCE
    if("${ARGC}" GREATER 1)
        set(${var_} ${ARGN})
        if(NOT ("${ARGV2}" STREQUAL "CACHE"))
            set(${var_} "${ARGV1}" PARENT_SCOPE)
        endif()
    endif()
    
    set(var_value "${${var_}}")
    string(TOUPPER "${var_value}" up_value)
    
    if(("${up_value}" STREQUAL "ON") OR ("${up_value}" STREQUAL "YES") OR
       ("${up_value}" STREQUAL "TRUE") OR ("${up_value}" STREQUAL "Y"))
        add_definitions(-D${var_}=1)
    elseif(("${up_value}" STREQUAL "OFF") OR ("${up_value}" STREQUAL "NO") OR
           ("${up_value}" STREQUAL "FALSE") OR ("${up_value}" STREQUAL "N") OR
           ("${up_value}" STREQUAL "IGNORE") OR ("${up_value}" STREQUAL "NOTFOUND"))
        add_definitions(-D${var_}=0)
    else()
        add_definitions(-D${var_}=${var_value})
    endif()
endfunction(forward)

# ---

function(forward_string var_) # value_ CACHE type_ docstring_ FORCE
    if("${ARGC}" GREATER 1)
        set(${var_} ${ARGN})
        if(NOT ("${ARGV2}" STREQUAL "CACHE"))
            set(${var_} "${ARGV1}" PARENT_SCOPE)
        endif()
    endif()
    
    add_definitions(-D${var_}="${${var_}}")
endfunction(forward_string)

# ---

function(install_directory dir_) # DESTINATION dest_dir_ COMPONENT component_
    # Parameters
    string(REGEX REPLACE "^/|/$" "" dir_ "${dir_}")
    cmake_parse_arguments(arg "" "DESTINATION;COMPONENT" "" ${ARGN})
    
    if(NOT arg_DESTINATION)
        set(arg_DESTINATION "${dir_}")
    endif()
    if(NOT arg_COMPONENT)
        set(arg_COMPONENT "Binary")
    endif()
    
    string(MAKE_C_IDENTIFIER "${dir_}" dir_identifier__)
    get_filename_component(dir_ "${dir_}" ABSOLUTE)
    
    # Make and install this
    if(NOT EXISTS "${dir_}")
        file(MAKE_DIRECTORY "${dir_}")
    endif()
    
    install(DIRECTORY "${dir_}/"
        DESTINATION "${arg_DESTINATION}" COMPONENT "${arg_COMPONENT}"
        USE_SOURCE_PERMISSIONS
    )
    
    # Set and forward `<dir>_DIR` variable
    if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        set(var_dir "${dir_}")
    elseif(IS_ABSOLUTE "${arg_DESTINATION}")
        set(var_dir "${arg_DESTINATION}")
    else()
        set(var_dir "${CMAKE_INSTALL_PREFIX}/${arg_DESTINATION}")
    endif()
    
    set(${dir_identifier__}_DIR "${var_dir}" PARENT_SCOPE)
    add_definitions(-D${dir_identifier__}_DIR="${var_dir}")
endfunction(install_directory)

# ---

function(get_sources)
    list(APPEND CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${LIB_DESTINATION}")
    
    # Automatic configure files from config/ to project root
    file(GLOB_RECURSE config_files__
        RELATIVE "${CONFIG_DIR}/" "${CONFIG_DIR}/*.in"
    )
    foreach(config_file__ ${config_files__})
        string(REGEX REPLACE "\\.in$" "" output_file__ "${config_file__}")
        
        configure_file("${CONFIG_DIR}/${config_file__}"
            "${PROJECT_SOURCE_DIR}/${output_file__}"
            @ONLY
        )
    endforeach(config_file__)
    
    # Start collect targets from src/CMakeLists.txt
    if(EXISTS "${SRC_DIR}/CMakeLists.txt")
        add_subdirectory("${SRC_DIR}")
    endif()
endfunction(get_sources)

#--------------------------------------------------------------#
#                  Bred-Soft resources module                  #
#--------------------------------------------------------------#

if(DEFINED __BredSoft_included) # - include guard
    return()
endif()


set(RES_INCLUDE_DIR "${PROJECT_BINARY_DIR}/Resources")

include_directories("${RES_INCLUDE_DIR}/")


set(__RESOURCES_DUMMY_SOURCE "${RES_INCLUDE_DIR}/dummy.c")

if(NOT EXISTS "${__RESOURCES_DUMMY_SOURCE}")
    file(WRITE "${__RESOURCES_DUMMY_SOURCE}" "")
endif()


# ---

function(add_resource_library lib_name_) # files_...
    set(objects_dir "${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/${lib_name_}.dir")
    set(lib_header "${RES_INCLUDE_DIR}/${lib_name_}.h")
    string(MAKE_C_IDENTIFIER "${lib_name_}" m_lib_name)
    
    # Resources
    
    file(WRITE "${lib_header}"
"#ifndef RESOURCES_${m_lib_name}_H__
#define RESOURCES_${m_lib_name}_H__

#ifdef __cplusplus
# include <string>
#endif // __cplusplus
"   )
    
    set(res_list)
    
    foreach(file_in__ ${ARGN})
        set(file_out__ "${objects_dir}/${file_in__}.o")
        string(MAKE_C_IDENTIFIER "${file_in__}" res_name__)
        
        get_filename_component(file_out_dir__ "${file_out__}" DIRECTORY)
        file(MAKE_DIRECTORY "${file_out_dir__}")
        
        add_custom_command(
            OUTPUT "${file_out__}"
            DEPENDS "${file_in__}"
            COMMAND "${CMAKE_LINKER}" -r -b binary -o "${file_out__}" "${file_in__}" # --leading-underscore
            WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
            COMMENT "Building resource '${res_name__}'"
            VERBATIM
        )
        list(APPEND res_list "${file_out__}")
        
        file(APPEND "${lib_header}" "

extern char const _binary_${res_name__}_start, _binary_${res_name__}_end;

#define ${res_name__}      (&_binary_${res_name__}_start)
#define ${res_name__}_end  (&_binary_${res_name__}_end)
#define ${res_name__}_size (${res_name__}_end - ${res_name__})

#ifdef __cplusplus
inline std::string ${res_name__}_string()
{   return std::string(${res_name__}, ${res_name__}_end);   }
#endif // __cplusplus
"       )
    endforeach(file_in__)
    
    file(APPEND "${lib_header}" "

#endif // RESOURCES_${m_lib_name}_H__
"   )
    
    # Library
    
    add_library(${lib_name_} STATIC
        "${__RESOURCES_DUMMY_SOURCE}"
        "${lib_header}"
        ${res_list}
    )
    target_compile_options(${lib_name_} PRIVATE -O3 -DNDEBUG)
endfunction(add_resource_library)

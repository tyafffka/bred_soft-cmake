#--------------------------------------------------------------#
#                 Bred-Soft module for targets                 #
#--------------------------------------------------------------#

if(DEFINED __BredSoft_included) # - include guard
    return()
endif()


set(EXPORT_DESTINATION "lib/cmake/${PROJECT_MAJOR_NAME}")

set(_EXPORT_DIR "${PROJECT_BINARY_DIR}/CMakeFiles/Export/${EXPORT_DESTINATION}")
string(TOLOWER "${PROJECT_NAME}" _EXPORT_NAME)

set(_EXPORT_CONFIG_IN "${CMAKE_CURRENT_LIST_DIR}/bs_config.cmake.in")
set(_EXPORT_VERSION_IN "${CMAKE_CURRENT_LIST_DIR}/bs_config-version.cmake.in")

# ---

function(target_resources target_) # files_...
    set(files__ "")
    foreach(file__ ${ARGN})
        get_filename_component(file__ "${file__}" ABSOLUTE)
        list(APPEND files__ "${file__}")
    endforeach(file__)
    
    set_property(TARGET ${target_} APPEND PROPERTY RESOURCE ${files__})
endfunction(target_resources)

# ---

function(target_public_headers target_) # headers_...
    set(headers__ "")
    foreach(header__ ${ARGN})
        get_filename_component(header__ "${header__}" ABSOLUTE)
        list(APPEND headers__ "${header__}")
    endforeach(header__)
    
    set_property(TARGET ${target_} APPEND PROPERTY PUBLIC_HEADER ${headers__})
endfunction(target_public_headers)

# ---

function(get_export_name var_ component_)
    if(NOT component_ OR "${component_}" STREQUAL "Binary")
        set(${var_} "${_EXPORT_NAME}-targets" PARENT_SCOPE)
    else()
        set(${var_} "${_EXPORT_NAME}-${component_}-targets" PARENT_SCOPE)
    endif()
endfunction(get_export_name)

# ---

function(common_install target_) # DESTINATION destination_ COMPONENT component_ NO_VERSION OPTIONAL
    cmake_parse_arguments(arg "NO_VERSION;OPTIONAL" "DESTINATION;COMPONENT" "" ${ARGN})
    
    if(PROJECT_VERSION AND NOT arg_NO_VERSION)
        set_target_properties(${target_} PROPERTIES
            VERSION ${PROJECT_VERSION}
            SOVERSION ${PROJECT_VERSION_MAJOR}
        )
    endif()
    
    if(arg_DESTINATION)
        set(bin_destination__ "${arg_DESTINATION}")
        set(lib_destination__ "${arg_DESTINATION}")
    else()
        set(bin_destination__ "${BIN_DESTINATION}")
        set(lib_destination__ "${LIB_DESTINATION}")
    endif()
    if(arg_COMPONENT)
        set(export__        "${_EXPORT_NAME}-${arg_COMPONENT}-targets")
        set(component__     "${arg_COMPONENT}")
        set(bin_component__ "${arg_COMPONENT}")
    else()
        set(export__        "${_EXPORT_NAME}-targets")
        set(component__     "Develop")
        set(bin_component__ "Binary")
    endif()
    if(arg_OPTIONAL)
        set(optional__ OPTIONAL)
    else()
        set(optional__)
    endif()
    
    get_target_property(public_headers__ ${target_} PUBLIC_HEADER)
    set_property(TARGET ${target_} PROPERTY PUBLIC_HEADER "")
    
    install(TARGETS ${target_} EXPORT "${export__}"
        INCLUDES DESTINATION "${INCLUDE_DESTINATION}"
        ARCHIVE DESTINATION "${lib_destination__}" COMPONENT "${component__}" ${optional__}
        LIBRARY DESTINATION "${lib_destination__}" COMPONENT "${bin_component__}" ${optional__}
        FRAMEWORK DESTINATION "${lib_destination__}" COMPONENT "${bin_component__}" ${optional__}
        RUNTIME DESTINATION "${bin_destination__}" COMPONENT "${bin_component__}" ${optional__}
        BUNDLE DESTINATION "${bin_destination__}" COMPONENT "${bin_component__}" ${optional__}
    )
    
    set_property(TARGET ${target_} PROPERTY PUBLIC_HEADER "${public_headers__}")
    
    # install resources
    
    get_target_property(resources__ ${target_} RESOURCE)
    
    if(resources__)
        foreach(file__ ${resources__})
            get_filename_component(destination__ "${file__}" DIRECTORY)
            file(RELATIVE_PATH destination__ "${PROJECT_SOURCE_DIR}/" "${destination__}/")
            
            if("${destination__}" MATCHES "^\\.\\./")
                set(destination__ "")
            endif()
            string(REGEX REPLACE "/$" "" destination__ "${destination__}")
            
            install(FILES "${file__}"
                DESTINATION "${destination__}" COMPONENT "${component__}" ${optional__}
            )
        endforeach(file__)
    endif()
    
    # special install headers saved folders stucture
    
    get_target_property(is_framework__ ${target_} FRAMEWORK)
    if(OSX AND ${is_framework__})
        return() # but not for OSX framework (see PUBLIC_HEADER target property).
    endif()
    if(NOT public_headers__)
        return()
    endif()
    
    get_target_property(searchs__ ${target_} INCLUDE_DIRECTORIES)
    
    foreach(header__ ${public_headers__})
        get_filename_component(header_dir__ "${header__}" DIRECTORY)
        set(destination__ "${INCLUDE_DESTINATION}")
        
        foreach(search__ ${searchs__})
            file(RELATIVE_PATH rel_header_dir__ "${search__}/" "${header_dir__}/")
            
            if(NOT ("${rel_header_dir__}" MATCHES "^\\.\\./"))
                set(destination__ "${INCLUDE_DESTINATION}/${rel_header_dir__}")
                break()
            endif()
        endforeach(search__)
        
        string(REGEX REPLACE "/$" "" destination__ "${destination__}")
        
        install(FILES "${header__}"
            DESTINATION "${destination__}" COMPONENT "${component__}" ${optional__}
        )
    endforeach(header__)
endfunction(common_install)

# ---

function(export_set var_) # value_
    if("${ARGC}" GREATER 1)
        set(value_ "${ARGV1}")
    else()
        set(value_ "${var_}")
    endif()
    
    string(REPLACE "\\" "\\\\" value_ "${value_}")
    string(REPLACE "\"" "\\\"" value_ "${value_}")
    string(REPLACE "\n" "\\n" value_ "${value_}")
    
    get_property(_EXPORT_ADDITIONAL_VARS GLOBAL PROPERTY _${PROJECT_NAME}_EXPORT_ADDITIONAL_VARS)
    set_property(GLOBAL PROPERTY _${PROJECT_NAME}_EXPORT_ADDITIONAL_VARS
        "${_EXPORT_ADDITIONAL_VARS}\nset(${var_} \"${value_}\")"
    )
endfunction(export_set)

# ---

function(install_export) # MAIN component_ ADDITIONAL components_...
    cmake_parse_arguments(arg "" "MAIN" "ADDITIONAL" ${ARGN})
    
    if(NOT arg_MAIN)
        set(arg_MAIN "Develop")
    endif()
    
    install(EXPORT "${_EXPORT_NAME}-targets"
        DESTINATION "${EXPORT_DESTINATION}"
        EXPORT_LINK_INTERFACE_LIBRARIES
        COMPONENT "${arg_MAIN}"
    )
    
    foreach(component__ ${arg_ADDITIONAL})
        set(_EXPORT_COMPONENTS "${_EXPORT_COMPONENTS} \"${component__}\"")
        
        install(EXPORT "${_EXPORT_NAME}-${component__}-targets"
            DESTINATION "${EXPORT_DESTINATION}"
            EXPORT_LINK_INTERFACE_LIBRARIES
            COMPONENT "${component__}"
        )
    endforeach()
    
    # main export
    
    get_property(_EXPORT_ADDITIONAL_VARS GLOBAL PROPERTY _${PROJECT_NAME}_EXPORT_ADDITIONAL_VARS)
    set_property(GLOBAL PROPERTY _${PROJECT_NAME}_EXPORT_ADDITIONAL_VARS "")
    
    configure_file("${_EXPORT_CONFIG_IN}"
        "${_EXPORT_DIR}/${_EXPORT_NAME}-config.cmake" @ONLY
    )
    install(FILES "${_EXPORT_DIR}/${_EXPORT_NAME}-config.cmake"
        DESTINATION "${EXPORT_DESTINATION}"
        COMPONENT "${arg_MAIN}"
    )
    
    # version export
    
    if(PROJECT_VERSION)
        configure_file("${_EXPORT_VERSION_IN}"
            "${_EXPORT_DIR}/${_EXPORT_NAME}-config-version.cmake" @ONLY
        )
        install(FILES "${_EXPORT_DIR}/${_EXPORT_NAME}-config-version.cmake"
            DESTINATION "${EXPORT_DESTINATION}"
            COMPONENT "${arg_MAIN}"
        )
    endif()
endfunction(install_export)
